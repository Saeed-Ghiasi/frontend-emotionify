import React from 'react';
import M from 'materialize-css';
import { Link } from 'react-router-dom';
import './NaivePlaylist.css';

class NaivePlaylist extends React.Component {

    error = () => {
        M.toast({
            html: 'Please choose an emotion!' ,
            classes: 'toast-express'
        })
    }

    makeEmojies = () => {
        var emotionSet = [
            {emotion:"happiness", emoji:"😄"} , 
            {emotion:"neutral", emoji:"😐"} ,
            {emotion:"sadness", emoji:"😔"} ,
            {emotion:"contempt", emoji:"😏"} , 
            {emotion:"surprise", emoji:"😲"} ,
            {emotion:"disgust", emoji:"🤢"} ,
            {emotion:"fear", emoji:"😨"} ,
            {emotion:"anger", emoji:"😡"} ,
        ];
        var buttons = emotionSet.map((item,index) => {
            return (
                <button key={index} className="btn-emoji">
                    <Link to={{
                            pathname:"recommendation",
                            data:
                            {
                                fetchData: [item.emotion],
                                fetchURL: 'http://127.0.0.1:8000/recommend/emotion/',
                                type: "emotion"
                            }
                    }} className="emojis" key={item.emotion}>
                        {item.emoji}
                    </Link>
                </button>
            )
        })
        return buttons;
    }

    render() {
        const buttons = this.makeEmojies();
        return (
            <center>
                <div className="container">
                    <div className="row black-text">
                        <div className="container">
                            <p>Or express your emotion for us using one of these emojis.</p>
                        </div>
                        <div class="col s12 m12 l12" style={{display: "inline-block", padding:"5%"}}>
                            {buttons}
                        </div>
                    </div>
                </div>
            </center>
        )
    }
}

export default NaivePlaylist;