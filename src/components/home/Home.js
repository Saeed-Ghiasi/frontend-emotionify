import React from 'react';
import { Link, Redirect } from 'react-router-dom';

class Layout extends React.Component {
    
    render() {
        var authenticated = localStorage.getItem('auth');
        console.log(authenticated);
        let or, connect;
        if (authenticated) {
            return <Redirect to='/select'/>
        } else {
            or = <h3>OR</h3>
            connect = (
                    <Link className="btn-large black" to="/sign-in">Connect With Spotify<i className="material-icons left">music_note</i></Link>
            )
        }

        return (
            <center>

            <div className="container center">
                <div className="row">
                    <div 
                        className="z-depth-1 blue-grey lighten-5 col s12 m8 l8 offset-m2 offset-l2 black-text " 
                        style={{display: "inline-block", marginTop:"2%", padding:"4%"}}
                    >
                        <div>
                            <h1>Welcome</h1>
                        </div>
                        <div>
                            <Link className="btn-large black" to="/select">Test It For Free!</Link>
                        </div>
                        <div>
                            {or}
                        </div>
                        <div>
                            {connect}
                        </div>
                    </div>
                </div>
            </div>
            </center>
        )
    }     
}

export default Layout;