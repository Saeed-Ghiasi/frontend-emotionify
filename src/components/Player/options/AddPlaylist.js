import React from 'react';
import './AddPlaylist.css';
import M from "materialize-css";

class AddPlaylist extends React.Component {

    like_playlist() {
        if (!localStorage.getItem('auth')) {
            M.toast({
                html: 'You must login to add playlist to your library!',
            })
            return
        }

        console.log(this.props.playlist_uri)
        var uploadData = new FormData();
        uploadData.append("playlist_id", this.props.playlist_uri)
        fetch("http://127.0.0.1:8000/like/playlist/", {
            method: 'POST',
            body: uploadData,
            headers: {'Authorization': 'Token '.concat(localStorage.getItem('token'))}
        })
            .then(response => {
                if (!response.ok) {
                    throw response.statusText;
                }
                return response
            })
            .then(response => response.json())
            .then(answer => {
                console.log(answer)
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <a className="add-playlist" onClick={() => this.like_playlist()}><i class="fas fa-plus"></i> Add Playlist to
                your library!</a>
        )
    }
}

export default AddPlaylist;
