import React from 'react';
import Player from '../../Player/Player';
import './History.css';
import M from 'materialize-css';

class History extends React.Component {
    state = {
        history: null,
        playlist_uri: null,
    }

    componentDidMount() {
        var elems = document.querySelectorAll('.dropdown-trigger');
        M.Dropdown.init(elems);
        
        this.get_history();
    }

    get_history = () => {
        fetch('http://127.0.0.1:8000/profile/', {
            method: 'GET',
            headers: { 'Authorization': 'Token '.concat(localStorage.getItem('token')) }
        })
        .then(response => response.json())
        .then(data => {
            this.setState({
                history: data
            })
        })
        .catch(err => console.log(err))
    }


    show_playlist(uri) {
        this.setState({
            playlist_uri: uri,
        })
    }

    render() {
        if (this.state.history != null) {
           var playlists = this.state.history.request_history.map(obj => {
                return (
                    <li key={obj.created_at} onClick={() => this.show_playlist(obj.playlist_id)}>playlist created at: {obj.created_at}</li>
                )
            })
        }

        var player = this.state.playlist_uri !== null ?
        <Player playlist_uri={this.state.playlist_uri} />
        :
        <h3 className="center">select your playlist</h3>

        return (
            <div>
                <div className="history-items"> 
                <div className="row">
                    <div className="col l8">
                        <a className="dropdown-trigger btn white black-text" href="#" data-target='dropdown-history'>
                            <i className="material-icons center">keyboard_arrow_down</i>
                            Choose playlist
                        </a>
                    </div>
                </div>
                    

                    <ul id='dropdown-history' className='dropdown-content'>
                        {playlists}
                    </ul>
                </div>
                <div className="history-playlist">
                    {player}
                </div>
            </div>
            
        )
    }
    
}

export default History;