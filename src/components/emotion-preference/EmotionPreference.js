import React from 'react';
import { Data } from './Data';
import RadioButtons from './RadioButtons';

class EmotionPreference extends React.Component {
    componentWillMount() {
        this.set_data();
        this.set_choices();
    }

    set_data = () => {
        this.setState({
            data: Data
        })
    }

    set_choices = () => {
        var choiceArray = new Array();
        Data.map(emotionObject => {
            var choiceObject = {emotion: emotionObject.emotionTag, happyOrSad: "default", calmOrEnergetic: "default"}

            choiceArray.push(choiceObject)
        }) 

        this.setState({
            choices: choiceArray
        },
        () => console.log(this.state.choices)
        )
    }

    update_choices = (newChoice) => {
        this.state.choices.map(item => {
            if (item.emotion === newChoice.emotion) {
                var updateChoice;
                if (newChoice.mappingEmotion === "Sad" || newChoice.mappingEmotion === "Happy")
                    updateChoice = {emotion: item.emotion, happyOrSad: newChoice.mappingEmotion, calmOrEnergetic: item.calmOrEnergetic};
                else if (newChoice.mappingEmotion === "Calm" || newChoice.mappingEmotion === "Energetic")
                    updateChoice = {emotion: item.emotion, happyOrSad: item.happyOrSad, calmOrEnergetic: newChoice.mappingEmotion};
                else updateChoice = item 

                var prevChoice = this.state.choices.filter(choice => !(item === choice));
                this.update_item(prevChoice, updateChoice);
            }
        })
    }

    update_item = (prevChoice, updatedChoice) => {
        this.setState({
            choices: prevChoice
        }, () => {
            this.setState({
                choices: [...this.state.choices, updatedChoice]
            })
        })
    }

    send_data = () => {
        var defaultCount = 0;
        for (var item of this.state.choices) {
            if (item.happyOrSad === "dafault" || item.calmOrEnergetic === "default") defaultCount = defaultCount + 1;
        }
        if (defaultCount == 0) {
            var uploadData = new FormData();
            uploadData.append('preferences', JSON.stringify(this.state.choices))
            
            fetch("http://127.0.0.1:8000/profile/preferences/" , {
                method: 'POST',
                body: uploadData,
                headers: { 'Authorization': 'Token '.concat(localStorage.getItem('token')) }
            })
            .then(data => {
                console.log(data);
                window.location.reload();
            })
            .catch(err => {
                window.alert("something went wrong")
                console.log(err)
            })
        }
        else {
            window.alert("Please Select All Options")
        }
    }

    reset = () => {
        fetch("http://127.0.0.1:8000/profile/preferences/" , {
            method: 'DELETE',
            headers: { 'Authorization': 'Token '.concat(localStorage.getItem('token')) }
        })
        .then(response => {
            if (response.ok) {
                window.alert("RESET WAS SUCCESSFULL")
                window.location.reload();
            }
            console.log(response)
        })
        .catch(err => {
            console.log(err)
            window.alert("something went wrong!")
        }
    )
    }

    render() {
        console.log(this.state.choices)
        var { data } = this.state;
        return (
            <div className="container">
                <h5 className="preference-title center">Which one do you prefer to listen when you are ...</h5>
                <ul className="radio-btns center row">
                    {data.map((emotionObject, emotionIndex) => {
                        return (
                            <li key={emotionIndex}>
                                <div className="col s12 m6 l6">
                                <div className="card blue-grey lighten-2">
                                    <div className="card-content white-text">
                                    <span className="card-title">{emotionObject.emotion}</span>
                                    <div>
                                        <RadioButtons key={emotionIndex} emotionObject={emotionObject} update_choices={this.update_choices} />
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </li>
                            
                        )
                    })}
                    <div className="col s12 m12 l12 center">
                        <a 
                            className="btn black white-text"
                            style={{marginRight:"2vw"}}
                            onClick={() => this.reset()}
                        >
                            Reset
                        </a>
                        <a
                            className="btn black white-text"
                            onClick={() => this.send_data()}
                        >
                            Submit
                        </a>
                    </div>
                </ul>
            </div>
        )
    }
}

export default EmotionPreference;