const SAD = {emotionName: "sad", emotionTagName: "sadness", mappingName:"Sad"}
const HAPPY = {emotionName: "happy", emotionTagName: "happiness", mappingName:"Happy"}
const NEUTRAL = {emotionName: "neutral", emotionTagName: "neutral", mappingName:"Neutral"}
const ANGER = {emotionName: "angry", emotionTagName: "anger", mappingName:"Angry"}
const ENERGETIC = {emotionName: "energetic", emotionTagName: "energetic", mappingName:"Energetic"}
const CALM = {emotionName: "calm", emotionTagName: "calm", mappingName:"Calm"}

const OPTIONS = [ [SAD, HAPPY], [CALM, ENERGETIC] ]

export const Data = [
    {
        emotion: SAD.emotionName,
        emotionTag: SAD.emotionTagName,
        options: OPTIONS
    },
    {
        emotion: NEUTRAL.emotionName,
        emotionTag: NEUTRAL.emotionTagName,
        options: OPTIONS
    },
    {
        emotion: HAPPY.emotionName,
        emotionTag: HAPPY.emotionTagName,
        options: OPTIONS
    },
    {
        emotion: ANGER.emotionName,
        emotionTag: ANGER.emotionTagName,
        options: OPTIONS
    },
]