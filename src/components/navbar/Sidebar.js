import React from 'react';
import { Link } from 'react-router-dom';
import M from "materialize-css/dist/js/materialize.min.js";
import './Sidebar.css';
import SignedInNav from './SignedInNav';
import SignedOutNav from './SignedOutNav';

class Sidebar extends React.Component {

    state = {
        auth: localStorage.getItem('auth')
    };

    componentDidMount() {
        let sidenav = document.querySelector('.sidenav');
        M.Sidenav.init(sidenav,{edge:'left'})
    }

    render() {
        var authenticated = this.state.auth ? <SignedInNav /> : <SignedOutNav /> ;

        return (
        <div>    
            <ul id="slide-out" className="sidenav">
                <li>
                    <div className="user-view">
                        <div className="background">
                        </div>  
                    </div>
                </li>
                <li className="side-li-home"><Link to="/">Home</Link></li>
                {authenticated}
            </ul>
        </div>
        )
    }
}

export default Sidebar;